//
//  LoginView.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import UIKit
import Stevia

protocol LoginViewDelegate {
    func onLoginTapped(userName: String)
}

class LoginView: UIView {

    var delegate: LoginViewDelegate? = nil

    let titleLabel = UILabel()
    let userNameText = UITextField()
    let loginButton = UIButton()
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    convenience init(frame: CGRect, delegate: LoginViewDelegate) {
        self.init(frame: frame)
        self.delegate = delegate
    }

    func initView() {
        
        sv(
            titleLabel.text("Github Dashboard")
                .style(labelStyle),
            activityIndicator,
            userNameText
                .placeholder("User name")
                .style(fieldStyle),
            loginButton
                .text("Search")
                .style(buttonStyle)
                .tap(loginTapped)
        )
        
        layout(
            100,
            |titleLabel.centerHorizontally().width(50%)|,
            "",
            |-20-userNameText.centerVertically()-20-| ~ 60,
            32,
            |loginButton.centerHorizontally().size(40%)-activityIndicator| ~ 60
        )
 
        userNameText.text = mainStore.state?.user.login ?? ""
        activityIndicator.hidesWhenStopped = true
    }
    
    func labelStyle(label: UILabel) {
        label.textColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        label.font = UIFont(name: "HelveticaNeue-Light", size: 26)
    }
    
    func fieldStyle(field: UITextField) {
        field.borderStyle = .roundedRect
        field.font = UIFont(name: "HelveticaNeue-Light", size: 26)
        field.returnKeyType = .next
    }

    func buttonStyle(button: UIButton) {
        button.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
    }

    func loginTapped() {
        let value = userNameText.text ?? ""
        delegate?.onLoginTapped(userName: value)
    }
    
    func showLoading(enable: Bool) {
        if (enable) {
            activityIndicator.startAnimating()
        }
        else {
            activityIndicator.stopAnimating()
        }
    }

}
