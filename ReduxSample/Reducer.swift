//
//  Reducer.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import ReSwift

struct MainReducer: Reducer {

    func handleAction(action: Action, state: AppState?) -> AppState {
        var state = state ?? AppState()
        
        switch action {
        case let userAction as SetUserAction:
            state.user = userAction.user
            state.userRequestStatus = .Done
        case let userRequestStatusAction as SetUserRequestStatusAction:
            state.userRequestStatus = userRequestStatusAction.status
        case let repositoriesAction as SetRepositoriesAction:
            state.repositories = repositoriesAction.repositories
        default:
            break
        }

        return state
    }


}
