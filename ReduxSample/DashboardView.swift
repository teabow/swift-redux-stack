//
//  DashboardView.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import UIKit
import Stevia
import ReSwift

class DashboardView: UIView, UITableViewDataSource {
    
    let titleLabel = UILabel()
    let userNameLabel = UILabel()
    let repositoriesTableView = UITableView()
    var user: User? = nil
    var repositories: [Repository]? = nil
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initView(user: nil)
    }
    
    convenience init(frame: CGRect, user: User) {
        self.init(frame: frame)
        initView(user: user)
    }

    func initView(user: User?) {
    
        let windowHeight = UIScreen.main.bounds.size.height
    
        let title = "\(user?.login ?? "")'s dashboard"
        let name = user?.name ?? ""
    
        sv(
            titleLabel.text(title).style(titleStyle),
            userNameLabel.text(name).style(labelStyle),
            repositoriesTableView
        )

        layout(
            100,
            |-20-titleLabel-20-| ~ 40,
            |-20-userNameLabel-20-| ~ 40,
            16,
            |-20-repositoriesTableView.width(100%)-20-| ~ windowHeight - 196
        )
        
        repositoriesTableView.dataSource = self
        repositoriesTableView.register(RepositoryCell.self, forCellReuseIdentifier: "RepoCell")
        repositoriesTableView.reloadData()
    }
    
    func titleStyle(label: UILabel) {
        label.font = UIFont(name: "HelveticaNeue-Light", size: 26)
    }
    
    func labelStyle(label: UILabel) {
        label.font = UIFont(name: "HelveticaNeue-Light", size: 20)
    }
    
    func setRepositories(repositories: [Repository]) {
        self.repositories = repositories;
        self.repositoriesTableView.reloadData()
    }
    
    // MARK: - Table view delegate -
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell", for: indexPath) as? RepositoryCell
        if (cell == nil) {
            cell = RepositoryCell(style: .default, reuseIdentifier: "RepoCell")
        }
        if let cellName = repositories?[indexPath.row].name {
            cell?.nameLabel.text = cellName
        }
        return cell!
    }
    
}
