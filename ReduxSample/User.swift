//
//  User.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import Arrow

struct User {
    var id: Int = -1
    var login: String = ""
    var name: String = ""
    var url: String = ""
    var publicRepos: Int = 0
    
    init() {}
    
    init(id: Int, login: String, name: String) {
        self.init()
        self.id = id
        self.login = login
        self.name = name
    }
}

extension User:ArrowParsable {
    mutating func deserialize(_ json: JSON) {
        id <-- json["id"]
        login <-- json["login"]
        name <-- json["name"]
        url <-- json["html_url"]
        publicRepos <-- json["public_repos"]
    }
}
