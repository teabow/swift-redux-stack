//
//  Repository.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 18/02/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import Arrow

struct Repository {
    var id = -1
    var name = ""
    var description = ""
    var language = ""
    var url = ""
}

extension Repository:ArrowParsable {
    mutating func deserialize(_ json: JSON) {
        id <-- json["id"]
        name <-- json["name"]
        language <-- json["language"]
        description <-- json["description"]
        url <-- json["html_url"]
    }
}
