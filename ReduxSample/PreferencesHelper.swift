//
//  PreferencesHelper.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation

class PreferencesHelper {

    let USER_ID_KEY = "user_id"
    let USER_LOGIN_KEY = "user_login"
    let USER_NAME_KEY = "user_name"

    public static let shared = PreferencesHelper()
    let defaults = UserDefaults.standard
    
    func storeUser(user: User) {
        defaults.set(user.id, forKey: USER_ID_KEY)
        defaults.set(user.login, forKey: USER_LOGIN_KEY)
        defaults.set(user.name, forKey: USER_NAME_KEY)
    }
    
    func getUser() -> User? {
        let hasUser = defaults.value(forKey: USER_ID_KEY) != nil
        if (hasUser) {
            let id = defaults.value(forKey: USER_ID_KEY) as! Int
            let login = defaults.value(forKey: USER_LOGIN_KEY) as! String
            let name = defaults.value(forKey: USER_NAME_KEY) as! String
            return User(id: id, login: login, name: name)
        }
        return nil
    }
    
}
