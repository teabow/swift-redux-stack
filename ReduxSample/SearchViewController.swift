//
//  ViewController.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import UIKit
import Stevia
import ReSwift

class SearchViewController: UIViewController, StoreSubscriber, LoginViewDelegate {

    var initState = true
    var loginView: LoginView? = nil

    // MARK: - Life cycle -

    override func viewWillAppear(_ animated: Bool) {
        mainStore.dispatch(SetUserRequestStatusAction(status: .Default))
        mainStore.subscribe(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        mainStore.unsubscribe(self)
    }
    
    // MARK: - View -
    
    func initView() {
        loginView = LoginView(frame: self.view.bounds, delegate: self)
        self.view.backgroundColor = #colorLiteral(red: 0.003037802409, green: 0.7368646264, blue: 0.8299823999, alpha: 1)
        self.view.addSubview(loginView!)
    }
    
    func onLoginTapped(userName: String) {
        loginView?.showLoading(enable: true)
        mainStore.dispatch(GetUserAction(userName: userName))
    }

    // MARK: - Redux -
    
    func navigateToDashboard() {
        loginView?.showLoading(enable: false)
        initState = true
        self.navigationController?.pushViewController(DashboardViewController(), animated: true)
    }

    func newState(state: AppState) {
        if (state.userRequestStatus == .Done) {
            navigateToDashboard()
        }
    }

}

