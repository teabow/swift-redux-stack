//
//  PersistMiddleware.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import ReSwift

let PersistMiddleware: Middleware = { dispatch, getState in
    return { next in
        return { action in
        
            if var action = action as? SetUserAction {
                PreferencesHelper.shared.storeUser(user: action.user)
            }
            return next(action)
            
        }
    }
}
