//
//  Actions.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import ReSwift

// MARK: - User actions -

struct SetUserAction: Action {
    let user: User
}
struct SetUserRequestStatusAction: Action {
    let status: RequestStatus
}
func GetUserAction(userName: String) -> Store<AppState>.ActionCreator {
    return { state, store in
        GithubApi.shared.getUser(userName: userName, completion: { user in
            DispatchQueue.main.async {
                store.dispatch(SetUserAction(user: user))
            }
        })
        return nil
    }
}

// MARK: - Repositories actions -

struct SetRepositoriesAction: Action {
    let repositories: [Repository]
}
func GetRepositoriesAction(userName: String) -> Store<AppState>.ActionCreator {
    return { state, store in
        GithubApi.shared.getRepositories(userName: userName, completion: { repositories in
            DispatchQueue.main.async {
                store.dispatch(SetRepositoriesAction(repositories: repositories))
            }
        })
        return nil
    }
}
