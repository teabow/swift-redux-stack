//
//  DashboardViewController.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import UIKit
import ReSwift

class DashboardViewController: UIViewController, StoreSubscriber {

    var dashboardView: DashboardView? = nil

    // MARK: - Life cycle -
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainStore.subscribe(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        mainStore.dispatch(GetRepositoriesAction(userName: mainStore.state.user.login))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    // MARK: - View -
    
    func initView() {
        let user = mainStore.state.user
        dashboardView = DashboardView(frame: self.view.bounds, user: user)
        self.view.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        self.view.addSubview(dashboardView!)
        
    }
    
    // MARK: - Redux -
    
    func newState(state: AppState) {
        if let repositories = state.repositories {
            dashboardView?.setRepositories(repositories: repositories)
        }
    }

}
