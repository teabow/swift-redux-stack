//
//  GithubApi.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import Alamofire
import Arrow

class GithubApi {

    public static let shared = GithubApi()

    let baseUrl: String = "https://api.github.com"
    let manager = Alamofire.SessionManager
   
    public func getUser(userName: String, completion: @escaping (_ user: User) -> Void) {
        let url = "\(baseUrl)/users/\(userName)"
        Alamofire.request(url).responseJSON { response in
            guard
                let value = response.result.value, let json = JSON(value)
            else {
                return
            }
            var user = User()
            user.deserialize(json)
            completion(user)
        }
    }
    
    public func getRepositories(userName: String, completion: @escaping (_ repositories: [Repository]) -> Void) {
        let url = "\(baseUrl)/users/\(userName)/repos"
        Alamofire.request(url).responseJSON { response in
            guard
                let value = response.result.value, let json = JSON(value), let jsonCollection = json.collection
            else {
                return
            }
            
            var repositories: [Repository] = []
            for item in jsonCollection {
                var repository = Repository()
                repository.deserialize(item)
                repositories.append(repository)
            }
            completion(repositories)
        }
    }
    

}

