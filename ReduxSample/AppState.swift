//
//  AppState.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 29/01/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import ReSwift

struct AppState: StateType {
    var user: User = PreferencesHelper.shared.getUser() ?? User()
    var userRequestStatus: RequestStatus = .Default
    var repositories: [Repository]? = nil
}
