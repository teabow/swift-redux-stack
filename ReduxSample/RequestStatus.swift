//
//  RequestStatus.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 19/02/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation

enum RequestStatus {
    case Default
    case Pending
    case Done
}
