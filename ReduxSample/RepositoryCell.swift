//
//  RepositoryCell.swift
//  ReduxSample
//
//  Created by Thibaud Bourgeois on 17/02/2017.
//  Copyright © 2017 Teabow. All rights reserved.
//

import Foundation
import UIKit
import Stevia

class RepositoryCell: UITableViewCell {
    
    let nameLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        sv(
            nameLabel.style(labelStyle)
        )
        
        layout(
            |-20-nameLabel.centerVertically()-20-|
        )
    }
    
    func labelStyle(label: UILabel) {
        label.font = UIFont(name: "HelveticaNeue-Light", size: 20)
    }
    
}
